Source: tpm2-tss
Section: libs
Priority: optional
Maintainer: Mathieu Trudel-Lapierre <cyphermox@ubuntu.com>
Uploaders: Ying-Chun Liu (PaulLiu) <paulliu@debian.org>,
           Ivan Hu <ivan.hu@ubuntu.com>,
           Mario Limonciello <superm1@gmail.com>
Build-Depends: acl,
               autoconf,
               autoconf-archive,
               debhelper-compat (= 13),
               docbook-xml,
               docbook-xsl,
               doxygen,
               dpkg-dev (>= 1.22.5),
               libcmocka-dev (>= 1.0),
               libcurl4-openssl-dev | libcurl-dev,
               libftdi-dev,
               libini-config-dev,
               libjson-c-dev,
               libltdl-dev,
               libssl-dev,
               libtool,
               libtpms-dev,
               libusb-1.0-0-dev,
               pkgconf,
               tpm-udev,
               uthash-dev,
               uuid-dev,
               xsltproc
Homepage: https://github.com/tpm2-software/tpm2-tss
Standards-Version: 4.5.0
Vcs-Browser: https://salsa.debian.org/debian/tpm2-tss
Vcs-Git: https://salsa.debian.org/debian/tpm2-tss.git

Package: libtss2-esys-3.0.2-0t64
Provides: ${t64:Provides}
Architecture: any
Depends: libtss2-tcti-cmd0t64,
         libtss2-tcti-device0t64,
         libtss2-tcti-mssim0t64,
         libtss2-tcti-swtpm0t64,
         tpm-udev,
         ${misc:Depends},
         ${shlibs:Depends}
Multi-Arch: same
Breaks: libtss2-esys-3.0.2-0 (<< ${source:Version}), libtss2-esys0 (<< 3.0.2-1)
Replaces: libtss2-esys-3.0.2-0, libtss2-esys0 (<< 3.0.2-1)
Description: TPM2 Software stack library - TSS and TCTI libraries
 TPM2.0 TSS (Software Stack) consists of API layers provided to support
 TPM 2.0 chips. It is made out of three layers:
 .
   - Enhanced System API (ESAPI)
   - System API (SAPI), which implements the system layer API;
   - Marshaling/Unmarshaling (MU)
   - TPM Command Transmission Interface (TCTI), which is used by SAPI to
     allow communication with the TAB/RM layer;
 .
 This package contains the TSS esys libraries that client applications
 will link against when they require accessing the TPM.

Package: libtss2-fapi1t64
Provides: ${t64:Provides}
Architecture: any
Depends: tpm-udev, ${misc:Depends}, ${shlibs:Depends}
Multi-Arch: same
Breaks: libtss2-esys0 (<< 3.0.1-2), libtss2-fapi1 (<< ${source:Version})
Replaces: libtss2-esys0 (<< 3.0.1-2), libtss2-fapi1
Description: TPM2 Software stack library - TSS and TCTI libraries
 TPM2.0 TSS (Software Stack) consists of API layers provided to support
 TPM 2.0 chips. It is made out of three layers:
 .
   - Enhanced System API (ESAPI)
   - System API (SAPI), which implements the system layer API;
   - Marshaling/Unmarshaling (MU)
   - TPM Command Transmission Interface (TCTI), which is used by SAPI to
     allow communication with the TAB/RM layer;
 .
 This package contains the TSS fapi libraries that client applications
 will link against when they require accessing the TPM.

Package: libtss2-mu-4.0.1-0t64
Provides: ${t64:Provides}
Architecture: any
Depends: tpm-udev, ${misc:Depends}, ${shlibs:Depends}
Multi-Arch: same
Breaks: libtss2-esys0 (<< 3.0.1-2), libtss2-mu-4.0.1-0 (<< ${source:Version})
Replaces: libtss2-esys0 (<< 3.0.1-2),
          libtss2-mu-4.0.1-0,
          libtss2-mu0 (<< 4.0.1-4)
Conflicts: libtss2-mu0
Description: TPM2 Software stack library - TSS and TCTI libraries
 TPM2.0 TSS (Software Stack) consists of API layers provided to support
 TPM 2.0 chips. It is made out of three layers:
 .
   - Enhanced System API (ESAPI)
   - System API (SAPI), which implements the system layer API;
   - Marshaling/Unmarshaling (MU)
   - TPM Command Transmission Interface (TCTI), which is used by SAPI to
     allow communication with the TAB/RM layer;
 .
 This package contains the TSS mu libraries that client applications
 will link against when they require accessing the TPM.

Package: libtss2-rc0t64
Provides: ${t64:Provides}
Architecture: any
Depends: tpm-udev, ${misc:Depends}, ${shlibs:Depends}
Multi-Arch: same
Breaks: libtss2-esys0 (<< 3.0.1-2), libtss2-rc0 (<< ${source:Version})
Replaces: libtss2-esys0 (<< 3.0.1-2), libtss2-rc0
Description: TPM2 Software stack library - TSS and TCTI libraries
 TPM2.0 TSS (Software Stack) consists of API layers provided to support
 TPM 2.0 chips. It is made out of three layers:
 .
   - Enhanced System API (ESAPI)
   - System API (SAPI), which implements the system layer API;
   - Marshaling/Unmarshaling (MU)
   - TPM Command Transmission Interface (TCTI), which is used by SAPI to
     allow communication with the TAB/RM layer;
 .
 This package contains the TSS rc libraries that client applications
 will link against when they require accessing the TPM.

Package: libtss2-sys1t64
Provides: ${t64:Provides}
Architecture: any
Depends: tpm-udev, ${misc:Depends}, ${shlibs:Depends}
Multi-Arch: same
Breaks: libtss2-esys0 (<< 3.0.1-2), libtss2-sys1 (<< ${source:Version})
Replaces: libtss2-esys0 (<< 3.0.1-2), libtss2-sys1
Description: TPM2 Software stack library - TSS and TCTI libraries
 TPM2.0 TSS (Software Stack) consists of API layers provided to support
 TPM 2.0 chips. It is made out of three layers:
 .
   - Enhanced System API (ESAPI)
   - System API (SAPI), which implements the system layer API;
   - Marshaling/Unmarshaling (MU)
   - TPM Command Transmission Interface (TCTI), which is used by SAPI to
     allow communication with the TAB/RM layer;
 .
 This package contains the TSS sys libraries that client applications
 will link against when they require accessing the TPM.

Package: libtss2-tcti-cmd0t64
Provides: ${t64:Provides}
Architecture: any
Depends: tpm-udev, ${misc:Depends}, ${shlibs:Depends}
Multi-Arch: same
Breaks: libtss2-esys0 (<< 3.0.1-2), libtss2-tcti-cmd0 (<< ${source:Version})
Replaces: libtss2-esys0 (<< 3.0.1-2), libtss2-tcti-cmd0
Description: TPM2 Software stack library - TSS and TCTI libraries
 TPM2.0 TSS (Software Stack) consists of API layers provided to support
 TPM 2.0 chips. It is made out of three layers:
 .
   - Enhanced System API (ESAPI)
   - System API (SAPI), which implements the system layer API;
   - Marshaling/Unmarshaling (MU)
   - TPM Command Transmission Interface (TCTI), which is used by SAPI to
     allow communication with the TAB/RM layer;
 .
 This package contains the TCTI cmd libraries that client applications
 will link against when they require accessing the TPM.

Package: libtss2-tcti-device0t64
Provides: ${t64:Provides}
Architecture: any
Depends: tpm-udev, ${misc:Depends}, ${shlibs:Depends}
Multi-Arch: same
Breaks: libtss2-esys0 (<< 3.0.1-2), libtss2-tcti-device0 (<< ${source:Version})
Replaces: libtss2-esys0 (<< 3.0.1-2), libtss2-tcti-device0
Description: TPM2 Software stack library - TSS and TCTI libraries
 TPM2.0 TSS (Software Stack) consists of API layers provided to support
 TPM 2.0 chips. It is made out of three layers:
 .
   - Enhanced System API (ESAPI)
   - System API (SAPI), which implements the system layer API;
   - Marshaling/Unmarshaling (MU)
   - TPM Command Transmission Interface (TCTI), which is used by SAPI to
     allow communication with the TAB/RM layer;
 .
 This package contains the TCTI device libraries that client applications
 will link against when they require accessing the TPM.

Package: libtss2-tcti-mssim0t64
Provides: ${t64:Provides}
Architecture: any
Depends: tpm-udev, ${misc:Depends}, ${shlibs:Depends}
Multi-Arch: same
Breaks: libtss2-esys0 (<< 3.0.1-2), libtss2-tcti-mssim0 (<< ${source:Version})
Replaces: libtss2-esys0 (<< 3.0.1-2), libtss2-tcti-mssim0
Description: TPM2 Software stack library - TSS and TCTI libraries
 TPM2.0 TSS (Software Stack) consists of API layers provided to support
 TPM 2.0 chips. It is made out of three layers:
 .
   - Enhanced System API (ESAPI)
   - System API (SAPI), which implements the system layer API;
   - Marshaling/Unmarshaling (MU)
   - TPM Command Transmission Interface (TCTI), which is used by SAPI to
     allow communication with the TAB/RM layer;
 .
 This package contains the TCTI mssim libraries that client applications
 will link against when they require accessing the TPM.

Package: libtss2-tcti-swtpm0t64
Provides: ${t64:Provides}
Architecture: any
Depends: tpm-udev, ${misc:Depends}, ${shlibs:Depends}
Multi-Arch: same
Breaks: libtss2-esys0 (<< 3.0.1-2), libtss2-tcti-swtpm0 (<< ${source:Version})
Replaces: libtss2-esys0 (<< 3.0.1-2), libtss2-tcti-swtpm0
Description: TPM2 Software stack library - TSS and TCTI libraries
 TPM2.0 TSS (Software Stack) consists of API layers provided to support
 TPM 2.0 chips. It is made out of three layers:
 .
   - Enhanced System API (ESAPI)
   - System API (SAPI), which implements the system layer API;
   - Marshaling/Unmarshaling (MU)
   - TPM Command Transmission Interface (TCTI), which is used by SAPI to
     allow communication with the TAB/RM layer;
 .
 This package contains the TCTI swtpm libraries that client applications
 will link against when they require accessing the TPM.

Package: libtss2-tcti-pcap0t64
Provides: ${t64:Provides}
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}
Multi-Arch: same
Breaks: libtss2-esys0 (<< 3.0.1-2), libtss2-tcti-pcap0 (<< ${source:Version})
Replaces: libtss2-esys0 (<< 3.0.1-2), libtss2-tcti-pcap0
Description: TPM2 Software stack library - TSS and TCTI libraries
 TPM2.0 TSS (Software Stack) consists of API layers provided to support
 TPM 2.0 chips. It is made out of three layers:
 .
   - Enhanced System API (ESAPI)
   - System API (SAPI), which implements the system layer API;
   - Marshaling/Unmarshaling (MU)
   - TPM Command Transmission Interface (TCTI), which is used by SAPI to
     allow communication with the TAB/RM layer;
 .
 This package contains the TCTI pcap libraries that client applications
 will link against when they require accessing the TPM.

Package: libtss2-tcti-spi-helper0t64
Provides: ${t64:Provides}
Replaces: libtss2-tcti-spi-helper0
Breaks: libtss2-tcti-spi-helper0 (<< ${source:Version})
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}
Multi-Arch: same
Description: TPM2 Software stack library - TSS and TCTI libraries
 TPM2.0 TSS (Software Stack) consists of API layers provided to support
 TPM 2.0 chips. It is made out of three layers:
 .
   - Enhanced System API (ESAPI)
   - System API (SAPI), which implements the system layer API;
   - Marshaling/Unmarshaling (MU)
   - TPM Command Transmission Interface (TCTI), which is used by SAPI to
     allow communication with the TAB/RM layer;
 .
 This package contains the TCTI spi helper libraries that client applications
 will link against when they require accessing the TPM.

Package: libtss2-tcti-spi-ftdi0
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}
Multi-Arch: same
Description: TPM2 Software stack library - TSS and TCTI libraries
 TPM2.0 TSS (Software Stack) consists of API layers provided to support
 TPM 2.0 chips. It is made out of three layers:
 .
   - Enhanced System API (ESAPI)
   - System API (SAPI), which implements the system layer API;
   - Marshaling/Unmarshaling (MU)
   - TPM Command Transmission Interface (TCTI), which is used by SAPI to
     allow communication with the TAB/RM layer;
 .
 This package contains the TCTI spi ftdi libraries that client applications
 will link against when they require accessing the TPM.

Package: libtss2-tcti-spi-ltt2go0
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}
Multi-Arch: same
Description: TPM2 Software stack library - TSS and TCTI libraries
 TPM2.0 TSS (Software Stack) consists of API layers provided to support
 TPM 2.0 chips. It is made out of three layers:
 .
   - Enhanced System API (ESAPI)
   - System API (SAPI), which implements the system layer API;
   - Marshaling/Unmarshaling (MU)
   - TPM Command Transmission Interface (TCTI), which is used by SAPI to
     allow communication with the TAB/RM layer;
 .
 This package contains the TCTI spi ltt2go libraries that client applications
 will link against when they require accessing the TPM.

Package: libtss2-tcti-spidev0
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}
Multi-Arch: same
Description: TPM2 Software stack library - TSS and TCTI libraries
 TPM2.0 TSS (Software Stack) consists of API layers provided to support
 TPM 2.0 chips. It is made out of three layers:
 .
   - Enhanced System API (ESAPI)
   - System API (SAPI), which implements the system layer API;
   - Marshaling/Unmarshaling (MU)
   - TPM Command Transmission Interface (TCTI), which is used by SAPI to
     allow communication with the TAB/RM layer;
 .
 This package contains the TCTI spidev libraries that client applications
 will link against when they require accessing the TPM.

Package: libtss2-tcti-i2c-helper0
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}
Multi-Arch: same
Description: TPM2 Software stack library - TSS and TCTI libraries
 TPM2.0 TSS (Software Stack) consists of API layers provided to support
 TPM 2.0 chips. It is made out of three layers:
 .
   - Enhanced System API (ESAPI)
   - System API (SAPI), which implements the system layer API;
   - Marshaling/Unmarshaling (MU)
   - TPM Command Transmission Interface (TCTI), which is used by SAPI to
     allow communication with the TAB/RM layer;
 .
 This package contains the TCTI i2c helper libraries that client applications
 will link against when they require accessing the TPM.

Package: libtss2-tcti-i2c-ftdi0
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}
Multi-Arch: same
Description: TPM2 Software stack library - TSS and TCTI libraries
 TPM2.0 TSS (Software Stack) consists of API layers provided to support
 TPM 2.0 chips. It is made out of three layers:
 .
   - Enhanced System API (ESAPI)
   - System API (SAPI), which implements the system layer API;
   - Marshaling/Unmarshaling (MU)
   - TPM Command Transmission Interface (TCTI), which is used by SAPI to
     allow communication with the TAB/RM layer;
 .
 This package contains the TCTI i2c ftdi libraries that client applications
 will link against when they require accessing the TPM.

Package: libtss2-tcti-libtpms0t64
Provides: ${t64:Provides}
Replaces: libtss2-tcti-libtpms0
Breaks: libtss2-tcti-libtpms0 (<< ${source:Version})
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}
Multi-Arch: same
Description: TPM2 Software stack library - TSS and TCTI libraries
 TPM2.0 TSS (Software Stack) consists of API layers provided to support
 TPM 2.0 chips. It is made out of three layers:
 .
   - Enhanced System API (ESAPI)
   - System API (SAPI), which implements the system layer API;
   - Marshaling/Unmarshaling (MU)
   - TPM Command Transmission Interface (TCTI), which is used by SAPI to
     allow communication with the TAB/RM layer;
 .
 This package contains the TCTI libtpms libraries that client applications
 will link against when they require accessing the TPM.

Package: libtss2-tctildr0t64
Provides: ${t64:Provides}
Architecture: any
Depends: libtss2-tcti-cmd0t64,
         libtss2-tcti-device0t64,
         libtss2-tcti-libtpms0t64,
         libtss2-tcti-mssim0t64,
         libtss2-tcti-spi-helper0t64,
         libtss2-tcti-swtpm0t64,
         tpm-udev,
         ${misc:Depends},
         ${shlibs:Depends}
Multi-Arch: same
Breaks: libtss2-esys0 (<< 3.0.1-2), libtss2-tctildr0 (<< ${source:Version})
Replaces: libtss2-esys0 (<< 3.0.1-2), libtss2-tctildr0
Description: TPM2 Software stack library - TSS and TCTI libraries
 TPM2.0 TSS (Software Stack) consists of API layers provided to support
 TPM 2.0 chips. It is made out of three layers:
 .
   - Enhanced System API (ESAPI)
   - System API (SAPI), which implements the system layer API;
   - Marshaling/Unmarshaling (MU)
   - TPM Command Transmission Interface (TCTI), which is used by SAPI to
     allow communication with the TAB/RM layer;
 .
 This package contains the TCTI ldr libraries that client applications
 will link against when they require accessing the TPM.

Package: libtss2-policy0t64
Provides: ${t64:Provides}
Replaces: libtss2-policy0
Breaks: libtss2-policy0 (<< ${source:Version})
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}
Multi-Arch: same
Description: TPM2 Software stack library - policy libraries
 TPM2.0 TSS (Software Stack) consists of API layers provided to support
 TPM 2.0 chips. It is made out of three layers:
 .
   - Enhanced System API (ESAPI)
   - System API (SAPI), which implements the system layer API;
   - Marshaling/Unmarshaling (MU)
   - TPM Command Transmission Interface (TCTI), which is used by SAPI to
     allow communication with the TAB/RM layer;
 .
 This package contains the policy libraries that client applications
 will link against when they require accessing the TPM.

Package: libtss2-dev
Architecture: any
Section: libdevel
Depends: libcurl4-openssl-dev | libcurl-ssl-dev,
         libjson-c-dev,
         libssl-dev,
         libtss2-esys-3.0.2-0t64 (= ${binary:Version}),
         libtss2-fapi1t64 (= ${binary:Version}),
         libtss2-mu-4.0.1-0t64 (= ${binary:Version}),
         libtss2-policy0t64 (= ${binary:Version}),
         libtss2-rc0t64 (= ${binary:Version}),
         libtss2-sys1t64 (= ${binary:Version}),
         libtss2-tcti-cmd0t64 (= ${binary:Version}),
         libtss2-tcti-device0t64 (= ${binary:Version}),
         libtss2-tcti-i2c-ftdi0 (= ${binary:Version}),
         libtss2-tcti-i2c-helper0 (= ${binary:Version}),
         libtss2-tcti-libtpms0t64 (= ${binary:Version}),
         libtss2-tcti-mssim0t64 (= ${binary:Version}),
         libtss2-tcti-pcap0t64 (= ${binary:Version}),
         libtss2-tcti-spi-ftdi0 (= ${binary:Version}),
         libtss2-tcti-spi-helper0t64 (= ${binary:Version}),
         libtss2-tcti-spi-ltt2go0 (= ${binary:Version}),
         libtss2-tcti-spidev0 (= ${binary:Version}),
         libtss2-tcti-swtpm0t64 (= ${binary:Version}),
         libtss2-tctildr0t64 (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends}
Multi-Arch: same
Description: TPM2 Software stack library - development files
 TPM2.0 TSS (Software Stack) consists of API layers provided to support
 TPM 2.0 chips. It is made out of three layers:
 .
   - Enhanced System API (ESAPI)
   - System API (SAPI), which implements the system layer API;
   - Marshaling/Unmarshaling (MU)
   - TPM Command Transmission Interface (TCTI), which is used by SAPI to
     allow communication with the TAB/RM layer;
 .
 This package contains development files for use when writing applications
 that need access to TPM chips.

Package: libtss2-doc
Architecture: all
Section: doc
Depends: ${misc:Depends}
Breaks: libtss2-dev (<< 3.2.0-5)
Replaces: libtss2-dev (<< 3.2.0-5)
Description: TPM2 Software stack library - documentation
 TPM2.0 TSS (Software Stack) consists of API layers provided to support
 TPM 2.0 chips.
 .
 This package contains documentation for use when writing applications
 that need access to TPM chips.
